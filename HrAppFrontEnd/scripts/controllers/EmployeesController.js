myApp.controller('EmployeesController', ['$scope', '$http', '$location', 'sessionFactory',  function($scope, $http, $location, sessionFactory){
    $scope.insertingMode = false;
    $scope.hireDate = new Date();
    $scope.employees = {};
    $scope.currentUserRole = sessionFactory.get("userRole").toLowerCase() === "user" || sessionFactory.get("userRole").toLowerCase() === "manager";

    $scope.getAllEmployees = function()
    {
        $http({url: 'http://localhost:8080/HrApp/rest/employees/getAllEmployees',
            method: 'GET',
            dataType:"json"
        })
            .success(function(data){
                $scope.employees = data;
            })
            .error(function(data){
                console.log("error" + data);
            });
    }();

    $scope.addEmployee = function()
    {
        $scope.insertingMode = true;
        $scope.userRole = sessionFactory.get("userRole");
        console.log("current user has role: " + $scope.userRole);
    };

    $scope.finish = function(name, email, password, department, manager, role, hireDate)
    {
        $scope.hireDate = hireDate;
        $http({url: 'http://localhost:8080/HrApp/rest/employees/addEmployee',
                method: 'POST',
                dataType:"json",
                data:{
                    name: name,
                    email: email,
                    password : password,
                    department : department,
                    manager: manager,
                    role: role,
                    hireDate: moment(new Date($scope.hireDate)).format('YYYY-MM-DD')
                }
            })
            .success(function(data){
                console.log(data);
            })
            .error(function(data){
                console.log(data);
            });
        $location.path('home');
    };

    $scope.viewProfile = function(employee)
    {
        var path = 'profile/' + employee._id.$oid;
        $location.path(path);
    };

    $scope.removeProfile = function(employee)
    {
        $http({url: 'http://localhost:8080/HrApp/rest/employees/deleteEmployee',
            method: 'POST',//not DELETE :(
            dataType:"json",
            data:{
                email: employee.email
            }
        })
            .success(function(data){
                $scope.employees.splice(employee, 1);
                console.log(data);
            })
            .error(function(data){
                console.log(data);
            });

    };

}]);