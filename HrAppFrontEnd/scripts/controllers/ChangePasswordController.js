myApp.controller('ChangePasswordController', ['$scope', '$http', '$location', 'sessionFactory', function($scope, $http, $location, sessionFactory){

$scope.changePassword = function(newPassword)
{
   // console.log("current user = " + sessionService.get("userLoggedIn") + " new pass: " + newPassword);
    $http({url: 'http://localhost:8080/HrApp/rest/password',
            method: 'POST',
            dataType:"json",
            data:{
                email:sessionFactory.get("userLoggedIn"),
                newPassword : newPassword
            }
        }
    )
        .success(function(data){
            console.log(data);
        })
        .error(function(data){
            console.log(data);
        });
};

}]);