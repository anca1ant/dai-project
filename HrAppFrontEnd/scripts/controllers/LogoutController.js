myApp.controller('LogoutController', ['$scope', '$location', 'sessionFactory', function($scope, $location, sessionFactory) {
    $scope.logout = function(){
        sessionFactory.destroy("userLoggedIn");
        sessionFactory.destroy("userRole");
    };
    $scope.logout();
    $scope.redirect = function(){
        $location.path("home");
    };
}]);
