myApp.controller('LoginController', ['$scope','$http', '$location', 'sessionFactory', function($scope, $http, $location, sessionFactory) {

    $scope.loginValid = true;
    $scope.userLogin = function(email, password){

        $http({url: 'http://localhost:8080/HrApp/rest/login',
                method: 'POST',
                dataType:"json",
                data:{
                    email : email,
                    password : password
                }
            })
            .success(function (data){
                if(data == ""){
                    $scope.loginValid = false;
                }
                else {
                    sessionFactory.set("userLoggedIn", email);
                    sessionFactory.set("userRole", data);
                    $location.path("home");
                }

            }).error(function(data){
               // console.log("post login error " + data);
                $scope.loginValid = false;
            }
        );
    };

}]);