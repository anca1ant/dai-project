myApp.controller('AdministrativeController', ['$scope', '$http', '$route', 'sessionFactory', function($scope, $http, $route, sessionFactory){
    $scope.currentUserEmail = sessionFactory.get("userLoggedIn");
    $scope.unapprovedRequests = {};

    $scope.getRequests = function()
    {
        $http({url: 'http://localhost:8080/HrApp/rest/administrative/getUnapprovedRequests',
            method: 'POST',
            dataType:"json",
            data:{
                email: $scope.currentUserEmail
            }
        })
            .success(function(data){
                $scope.unapprovedRequests = data;
            })
            .error(function(data){
                console.log(data);
            });

    }();

    $scope.approveRequest = function(leaveModel)
    {
        $http({url: 'http://localhost:8080/HrApp/rest/administrative/approveRequest',
            method: 'POST',
            dataType:"json",
            data:{
                email: leaveModel.email,
                dateFrom : leaveModel.dateFrom.$date,
                dateTo : leaveModel.dateTo.$date
            }
        })
            .success(function(data){
            })
            .error(function(data){
                console.log(data);
            });
        $route.reload();
    };

}]);