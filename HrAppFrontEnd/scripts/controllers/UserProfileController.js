myApp.controller('UserProfileController', ['$scope', '$http', '$location', '$routeParams', 'sessionFactory', function($scope, $http, $location, $routeParams, sessionFactory){
    $scope.email = sessionFactory.get("userLoggedIn");
    $scope.userProfile = {};
    $scope.userId = $routeParams.userId;

    $scope.getCurrentUserProfile = function()
    {
        $http({url: 'http://localhost:8080/HrApp/rest/employees/getCurrentUserProfile',
            method: 'POST',
            dataType:"json",
            data:{
                email: $scope.email
            }
        })
            .success(function(data){
                $scope.userProfile = data[0];
                console.log(data);
            })
            .error(function(data){
                console.log(data);
            });
    };

    $scope.getUserProfile = function()
    {
        $http({url: 'http://localhost:8080/HrApp/rest/employees/getUserProfile?userId=' + $scope.userId,
            method: 'GET'
        })
            .success(function(data){
                $scope.userProfile = data[0];
                console.log(data);
            })
            .error(function(data){
                console.log(data);
            });
    };

    $scope.userProfileController = function()
    {
        if($scope.userId == undefined)
        {
            $scope.getCurrentUserProfile();
            console.log("undefined is not defined");
        }else
        {
            $scope.getUserProfile();
        }

    }();
}]);