myApp.controller('RegisterController', ['$scope', '$http', '$location', function($scope, $http, $location){

    $scope.userNameAlreadyExists = false;
    $scope.passwordMismatch = false;

    $scope.registerUser = function(name, email, password, passwordConfirm){
        $scope.userNameAlreadyExists = false;//reset value
        $scope.passwordMismatch = password != passwordConfirm;

        if(!$scope.passwordMismatch)
        {
            $http({
                    url: 'http://localhost:8080/HrApp/rest/register',
                    method: 'POST',
                    dataType: "json",
                    data: {
                        name: name,
                        email: email,
                        password: password
                    }
                })
                .success(function(data){
                    if(data == "ok"){
                        console.log("register succeeded");
                        $location.path("login");
                    }
                    if(data == "failed"){
                        $scope.userNameAlreadyExists = true;
                        console.log("register failed");
                    }
                })
                .error(function(data){
                    console.log("server register failed" + data);
                });
        };
    }

}]);