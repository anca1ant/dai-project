myApp.controller('MenuController', ['$scope', 'sessionFactory', function($scope, sessionFactory){
    $scope.currentDate = new Date();
    $scope.show = function(){
        var userLoggedIn = sessionFactory.get("userLoggedIn") != null;
        return !userLoggedIn;
    };

    $scope.admin = function(){
        var role = sessionFactory.get("userRole");
        //console.log("role??" + role);
        var isManager = (role=="manager");

        return isManager;
    };
}]);