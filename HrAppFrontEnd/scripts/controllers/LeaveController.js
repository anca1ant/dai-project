myApp.controller('LeaveController', ['$scope', '$http', '$route', 'sessionFactory', function($scope, $http, $route, sessionFactory){
    $scope.dateFrom = moment(new Date());
    $scope.dateTo = moment(new Date());
    $scope.history = {};
    $scope.newRequest = false;
    $scope.validRequest = true;

    $scope.getUserHistory = function()
    {
        $http({url: 'http://localhost:8080/HrApp/rest/leave/getLeaveHistory',
            method: 'POST',
            dataType:"json",
            data:{
                email: sessionFactory.get("userLoggedIn")
            }
        })
            .success(function(data){
                $scope.history = data;
               // console.log(data);
            })
            .error(function(data){
                console.log(data);
            })

    }();

    $scope.addNewRequest = function()
    {
        $scope.newRequest = !($scope.newRequest);
    };

    $scope.getDate = function()
    {
        //.format('DD.MM.YYYY')
        $scope.validRequest = moment(new Date($scope.dateTo)) > moment(new Date($scope.dateFrom));
        console.log("valid request?? " + $scope.validRequest);

        if($scope.validRequest)
        {
            $http({url: 'http://localhost:8080/HrApp/rest/leave/addLeaveRequest',
                method: 'POST',
                dataType:"json",
                data:{
                    email: sessionFactory.get("userLoggedIn"),
                    dateFrom : moment(new Date($scope.dateFrom)).format('YYYY-MM-DD'),
                    dateTo : moment(new Date($scope.dateTo)).format('YYYY-MM-DD'),
                    approved : false
                }
            })
                .success(function(data){
                    //console.log(data);
                    $route.reload();
                })
                .error(function(data){
                    console.log(data);
                });
            $scope.newRequest = !($scope.newRequest);
        }

    }

}]);