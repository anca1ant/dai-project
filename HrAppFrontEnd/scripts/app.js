var myApp = angular.module('myApp', ['ngRoute', 'ngResource', 'ui.validate']);
myApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider
            .when('/home', {
                templateUrl: 'templates/home.html',
                controller: 'HomeController'
            })
            .when('/login', {
                templateUrl: 'templates/login.html',
                controller: 'LoginController'
            })
            .when('/logout', {
                templateUrl: 'templates/logout.html',
                controller: 'LogoutController'
            })
            .when('/register', {
                templateUrl: 'templates/register.html',
                controller: 'RegisterController'
            })
            .when('/profile/:userId?', {//optional route param
                templateUrl: 'templates/userProfile.html',
                controller: 'UserProfileController'
            })
            .when('/calendar',{
                templateUrl: 'templates/calendar.html',
                controller: 'CalendarController'
            })
            .when('/employees',{
                    templateUrl: 'templates/employees.html',
                    controller: 'EmployeesController'
            })
            .when('/changePassword', {
                templateUrl: 'templates/changePassword.html',
                controller: 'ChangePasswordController'
            })
            .when('/leave',{
                templateUrl: 'templates/leave.html',
                controller: 'LeaveController'
            })
            .when('/administrative',{
                templateUrl: 'templates/administrative.html',
                controller: 'AdministrativeController'
            })
            .otherwise({
                templateUrl: 'templates/home.html',
                controller: 'HomeController'
            });
    }]);


// DatePicker -> NgModel
myApp.directive('datePicker', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModel) {
            $(element).datetimepicker({
                format: 'DD.MM.YYYY',
                parseInputDate: function (data) {
                    if (data instanceof Date) {
                        return moment(data);
                    } else {
                        return moment(new Date(data));
                    }
                }
            });

            $(element).on("dp.change", function (e) {
                ngModel.$viewValue = e.date;
                ngModel.$commitViewValue();
            });
        }
    };
});

// DatePicker Input NgModel->DatePicker
myApp.directive('datePickerInput', function() {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModel) {
            scope.$watch(attr.ngModel, function (value) {
                if (value) {
                    element.trigger("change");
                }
            });
        }
    };
});