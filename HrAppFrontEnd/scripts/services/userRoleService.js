myApp.service('userRoleService', [ function(){
    var userHasRole = false;

    this.login = function(){
        userHasRole = true;
        return userHasRole;
    };
    
    this.logout = function(){
        userHasRole = false;
        return userHasRole;
    };
    this.value = function(){
        return userHasRole;
    };
}]);