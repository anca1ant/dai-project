myApp.service('userLoggedService', [ function(){
    var userLoggedIn = false;
    this.login = function(){
        userLoggedIn = true;
        return userLoggedIn;
    };
    this.logout = function(){
        userLoggedIn = false;
        return userLoggedIn;
    };
    this.value = function(){
        return userLoggedIn;
    }
}]);