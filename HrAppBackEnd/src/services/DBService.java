package services;

import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import models.LeaveModel;
import models.PasswordModel;
import models.UserProfile;

public class DBService {

	private MongoClient mongoClient;
	private DB database;
	private DBCollection usersCollection;
	private DBCollection leaveCollection;
	
	public DBService(){	
		try {
			mongoClient = new MongoClient("localhost", 27017);
			database = mongoClient.getDB("hrDatabase");
			usersCollection = database.getCollection("userProfile");
			leaveCollection = database.getCollection("leave");
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	public Boolean userExists(String email){
		
		BasicDBObject query = new BasicDBObject("email", email);
		DBObject userDB =  usersCollection.findOne(query);
		
		if(userDB == null){
			System.out.println("userul " + email + " nu exista");
			return false;
		}else{
			System.out.println("userul " + email + " exista!");
			return true;
		}
	}
	
	public void registerUser(UserProfile userProfile){
		
		BasicDBObject doc = null;
		try {
			String salt = Util.getSalt();
			doc = new BasicDBObject("email", userProfile.getEmail())
					.append("userName", userProfile.getName())
					.append("salt", salt)
					.append("role", "admin")
					.append("password", Util.password_to_md5(userProfile.getPassword(), salt));
			usersCollection.insert(doc);
		
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteEmployee(UserProfile userProfile)
	{
		BasicDBObject doc = new BasicDBObject("email", userProfile.getEmail());
		usersCollection.remove(doc);
		leaveCollection.remove(doc);
	}
	
	public String getUsername(UserProfile userProfile){
		
		BasicDBObject query = new BasicDBObject("email", userProfile.getEmail());
		DBObject userDB =  usersCollection.findOne(query);
		String username = null;
		
		if(userDB != null){
			username = (String)userDB.get("name");
		}
		return username;
	}
	
	public String userLogin(UserProfile userProfile){
		
		BasicDBObject query = new BasicDBObject("email", userProfile.getEmail());
		DBObject userDB =  usersCollection.findOne(query);
		
		if(userDB == null)
		{
			return "";
		}
		
		String DBSalt = (String) userDB.get("salt");
		String DBPassword = (String) userDB.get("password");
		String introducedPassword = Util.password_to_md5(userProfile.getPassword(), DBSalt);		
		if(DBPassword.equals(introducedPassword))
			return (String)userDB.get("role");
		else
			return "";
		
	}	

	public void changeUserPassword(PasswordModel password)
	{
		BasicDBObject query = new BasicDBObject("email", password.getEmail());
		DBObject userDB =  usersCollection.findOne(query);
		
		if(userDB != null)
		{
			DBObject userToUpdate = new BasicDBObject();
			
			try {
				String salt = Util.getSalt();
				BasicDBObject update = new BasicDBObject("salt", salt)
										.append("password", Util.password_to_md5(password.getNewPassword(), salt));
				
				userToUpdate.put("$set", update);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			
			usersCollection.update(query, userToUpdate);
		}
	}

	public void addEmployee(UserProfile userProfile)
	{
		BasicDBObject doc = new BasicDBObject("email", userProfile.getEmail());
		try
		{
			String salt = Util.getSalt();
			doc.append("name",userProfile.getName())
					.append("salt", salt)
					.append("password", Util.password_to_md5(userProfile.getPassword(), salt))
					.append("department", userProfile.getDepartment())
					.append("manager", userProfile.getManager())
					.append("role", userProfile.getRole())
					.append("hireDate", userProfile.getHireDate());
			usersCollection.insert(doc);
		}catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}
	
	public List<String> getCurrentUserProfile(UserProfile userProfile)
	{
		List<String> userDetails = new ArrayList<String>();
		
		BasicDBObject query = new BasicDBObject("email", userProfile.getEmail());
		DBCursor cursor =  usersCollection.find(query);
		
		while(cursor.hasNext()){
			userDetails.add(cursor.next().toString());
		}
		
		return userDetails;
	}
	
	public List<String> getUserProfile(String userId)
	{
		List<String> userDetails = new ArrayList<String>();
		BasicDBObject query = new BasicDBObject("_id", new ObjectId(userId));
		DBCursor cursor =  usersCollection.find(query);
		
		while(cursor.hasNext()){
			userDetails.add(cursor.next().toString());
		}
		
		return userDetails;
	}
	
	public List<String> getAllEmployees()
	{
		List<String> employees = new ArrayList<String>();
		DBCursor cursor =  usersCollection.find();
		
		while(cursor.hasNext()){
			employees.add(cursor.next().toString());
		}
		
		return employees;
	} 
	
	public void addHolidayRequest(LeaveModel newLeaveRequest)
	{
		BasicDBObject document = new BasicDBObject();
		document.put("email", newLeaveRequest.getEmail());
		document.put("dateFrom", newLeaveRequest.getDateFrom());
		document.put("dateTo", newLeaveRequest.getDateTo());
		document.put("approved", newLeaveRequest.getApproved());
		
		leaveCollection.insert(document);
	}
	
	public void updateHolidayRequest()
	{
		
	}
	
	public List<String> getLeaveHistory(LeaveModel model)
	{
		List<String> leaveHistory = new ArrayList<String>();
		
		BasicDBObject query = new BasicDBObject("email", model.getEmail());
		DBCursor cursor =  leaveCollection.find(query);
		
		while(cursor.hasNext()){
			leaveHistory.add(cursor.next().toString());
		}
		
		return leaveHistory;
	}
	
	public List<String> getAllLeaveRequests(String email)
	{
		BasicDBObject query = new BasicDBObject("manager", email);
		
		BasicDBObject keys = new BasicDBObject("email", 1);
		DBCursor cursor =  usersCollection.find(query,keys);
		
		List<String> users = new ArrayList<String>();
		
		while(cursor.hasNext()){
			String next = cursor.next().get("email").toString();
			users.add(next);
		}
		
		List<String> leaveHistory = new ArrayList<String>();
		
		for(String user : users)
		{
			BasicDBObject q2 = new BasicDBObject("email", user).append("approved", false);
			DBCursor c2 = leaveCollection.find(q2);
			
			while(c2.hasNext())
			{
				leaveHistory.add(c2.next().toString());
			}
		}
		return leaveHistory;
	}
	
	public void approveRequest(LeaveModel leaveModel)
	{
		BasicDBObject newDocument = new BasicDBObject();
		newDocument.put("approved", true);
		newDocument.put("email", leaveModel.getEmail());
		newDocument.put("dateFrom", leaveModel.getDateFrom());
		newDocument.put("dateTo", leaveModel.getDateTo());
		
		BasicDBObject oldDocument = new BasicDBObject()
								.append("email", leaveModel.getEmail())
								.append("dateFrom", leaveModel.getDateFrom())
								.append("dateTo", leaveModel.getDateTo());
		
		leaveCollection.update(oldDocument, newDocument);
	}
	
}
