package controllers;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import models.LeaveModel;
import services.DBService;

@Path("/leave")
public class LeaveController {

	private DBService dbService;

	public LeaveController(){
		dbService = new DBService();
	}
	
	@POST
	@Path("/getLeaveHistory")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLeaveHistory(LeaveModel userLeaveModel)
	{
		List<String> userHistory = dbService.getLeaveHistory(userLeaveModel);
		
		return Response
				.status(200)
				.type(MediaType.APPLICATION_JSON)
				.entity(userHistory.toString()).build();
	}
	
	@POST
	@Path("/addLeaveRequest")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addLeaveRequest(LeaveModel newRequest)
	{
		dbService.addHolidayRequest(newRequest);
		return Response
				.status(200)
				.type(MediaType.TEXT_PLAIN)
				.entity("ok").build();
	}
}
