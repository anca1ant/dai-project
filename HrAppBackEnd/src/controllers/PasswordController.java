package controllers;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;

import models.PasswordModel;
import services.DBService;

@Path("/password")
public class PasswordController {

	private DBService dbService;

	public PasswordController(){
		dbService = new DBService();
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response changePassword(PasswordModel passwordModel)
	{
		System.out.println("user " + passwordModel.getEmail() +" wants to channge current passwor to: " + passwordModel.getNewPassword());
		
		dbService.changeUserPassword(passwordModel);
		
		return Response
				.status(200)
				.type(MediaType.TEXT_PLAIN)
				.entity("todo").build();
	}
}
