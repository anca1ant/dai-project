package controllers;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import models.UserProfile;
import services.DBService;

@Path("/register")
public class RegisterController {

	private DBService dbService;
	
	public RegisterController(){
		dbService = new DBService();
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response userRegister(UserProfile userProfile){
		if(dbService.userExists(userProfile.getEmail()))
		{
			return Response
					.status(200)
					.type(MediaType.TEXT_PLAIN)
					.entity("failed").build();
		}
		else
		{
			dbService.registerUser(userProfile);
			return Response
					.status(200)
					.type(MediaType.TEXT_PLAIN)
					.entity("ok").build();
		}
			
	}
}
