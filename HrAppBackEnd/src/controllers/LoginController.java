package controllers;

import java.net.UnknownHostException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import services.DBService;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;

import models.UserProfile;

@Path("/login")
public class LoginController {

	private DBService dbService;

	public LoginController(){
		dbService = new DBService();
	}

	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	//@Produces(MediaType.APPLICATION_JSON)
	public Response userLogin(UserProfile userProfile){
		String loginAttempt = dbService.userLogin(userProfile);
		if(loginAttempt.equals("")){
			return Response
					.status(200)
					.type(MediaType.TEXT_PLAIN)
					.entity("").build();
		}
		return Response
				.status(200)
				.type(MediaType.TEXT_PLAIN)
				.entity(loginAttempt).build();

	}

	@POST
	@Path("/getUsernameFromMail")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUsername(UserProfile userProfile){
		String username = dbService.getUsername(userProfile);
		return Response.status(200).entity(username).build();
	}

	@GET
	@Path("/{param}")
	public Response getMsg(@PathParam("param") String userName) {
		try {
			MongoClient mongoClient = new MongoClient("localhost", 27017);
			List<String> dbs = mongoClient.getDatabaseNames();

			DB db = mongoClient.getDB("hrDatabase");
			DBCollection collection = db.getCollection("userProfile");
			BasicDBObject query = new BasicDBObject("userName", "Anca");
			DBCursor cursor = collection.find(query);

			try {
				while(cursor.hasNext()) {
					System.out.println("@@" + cursor.next());
				}
			} finally {
				cursor.close();
			}

		} catch (UnknownHostException e) {
			e.printStackTrace();
		} 

		return Response.status(200).entity("testMEE").build();
	}

}
