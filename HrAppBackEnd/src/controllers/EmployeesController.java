package controllers;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import models.UserProfile;
import services.DBService;

@Path("/employees")
public class EmployeesController {
	
	private DBService dbService;

	public EmployeesController(){
		dbService = new DBService();
	}
	
	@POST
	@Path("/addEmployee")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addEmployee(UserProfile employee)
	{
		System.out.println("Employee to add: " + employee.getName() + " " + employee.getManager() + " " + employee.getHireDate());
		dbService.addEmployee(employee);
		
		return Response
				.status(200)
				.type(MediaType.TEXT_PLAIN)
				.entity("todo").build();
	}
	
	@POST
	@Path("/deleteEmployee")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deleteEmployee(UserProfile employee)
	{
		dbService.deleteEmployee(employee);
		
		return Response
				.status(200)
				.type(MediaType.TEXT_PLAIN)
				.entity("ok").build();
	}
	
	@POST
	@Path("/getCurrentUserProfile")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)

	public Response getCurrentUserProfile(UserProfile employee)
	{
		List<String> userDetails = dbService.getCurrentUserProfile(employee);
		
		return Response
				.status(200)
				.type(MediaType.APPLICATION_JSON)
				.entity(userDetails.toString()).build();
	}
	
	@GET
	@Path("/getUserProfile")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserProfile(@QueryParam("userId") String userId)//todo
	{
		List<String> userDetails = dbService.getUserProfile(userId);
		
		return Response
				.status(200)
				.type(MediaType.APPLICATION_JSON)
				.entity(userDetails.toString()).build();
	}
	
	@GET
	@Path("/getAllEmployees")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllEmployees()
	{
		List<String> employees = dbService.getAllEmployees();

		return Response
				.status(200)
				.type(MediaType.APPLICATION_JSON)
				.entity(employees.toString()).build();
	}
	
}
