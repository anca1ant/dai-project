package controllers;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import models.LeaveModel;
import models.UserProfile;
import services.DBService;

@Path("/administrative")
public class AdministrativeController {
	
	private DBService dbService;

	public AdministrativeController(){
		dbService = new DBService();
	}
	
	@POST
	@Path("/getUnapprovedRequests")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllLeaveRequests(UserProfile userProfile)
	{
		List<String> requests = dbService.getAllLeaveRequests(userProfile.getEmail());

		return Response
				.status(200)
				.type(MediaType.APPLICATION_JSON)
				.entity(requests.toString()).build();
	}
	@POST
	@Path("/approveRequest")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response approveRequest(LeaveModel leaveModel)
	{
		dbService.approveRequest(leaveModel);

		return Response
				.status(200)
				.type(MediaType.TEXT_PLAIN)
				.entity("ok").build();
	}
		
	
}
