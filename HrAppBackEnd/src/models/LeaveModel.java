package models;

import java.util.Date;

public class LeaveModel {

	private String email;
	private Date dateFrom;
	private Date dateTo;
	private Boolean approved;

	public LeaveModel(){
	}

	public LeaveModel(String email, Date dateFrom, Date dateTo, Boolean approved) {
		super();
		this.email = email;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.approved = approved;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}
	
	
}
