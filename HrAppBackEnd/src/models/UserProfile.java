package models;

import java.util.Date;

public class UserProfile {
	
	private String name;
	private String email;
	private String password;
	private String department;
	private String manager;
	private String role;
	private Date hireDate;
	
	public UserProfile(){
	}

	public UserProfile(String name, String email, String password, String department, String manager, String role, Date hireDate) {
		this.name = name;
		this.email = email;
		this.password = password;
		this.department = department;
		this.manager = manager;
		this.role = role;
		this.hireDate = hireDate;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Date getHireDate() {
		return hireDate;
	}

	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}
	
}
